import React, { Component } from 'react';
import io from 'socket.io-client';
import Chat from './Chat'

class App extends Component {

  state = {
    isConnected: false,
    id: null,
    friends: [],
    answer: "",
    old_messages: []
  }

  socket = null

  componentDidMount() {
    this.socket = io('http://codicoda.com:8000');

    this.socket.on('connect', () => {
      this.setState({ isConnected: true })
    })

    this.socket.on('disconnect', () => {
      this.setState({ isConnected: false })
    })

    this.socket.on('room', (old_messages) => this.setState({ old_messages }))

    this.socket.on('pong!', (additionalStuff) => {
      console.log("server answered!", additionalStuff)
    })
    this.socket.on('youare', (answer) => {
      this.setState({ id: answer.id })
    })
    this.socket.on('next', (message_from_server) => console.log(message_from_server))

    this.socket.on('peeps', (friends) => this.setState({ friends }))

  }


  componentWillUnmount() {
    this.socket.console()
    this.socket = null
  }


  send = (text) => {
    const new_message = {
      id: this.state.id,
      name: "husseinh",
      text
    }
    this.socket.emit('message', new_message)
    let old_messages = this.state.old_messages
    old_messages = [...old_messages, new_message]
    this.setState({ old_messages })

  }
  render() {
    return (
      <div className="App">
        <div>status: {this.state.isConnected ? 'connected' : 'disconnected'}</div>
        <div>id: {this.state.id}</div>
        <button onClick={() => this.socket.emit('whoami')}>Who am I?</button>
        <hr />
        <Chat
          old_messages={this.state.old_messages}
          friends={this.state.friends}
          send={this.send}
        />
      </div>

    );
  }
}

export default App;
