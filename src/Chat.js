import React, { Component } from 'react';


class Chat extends Component {


  state = {
    text: ""
  }


  onSubmit = (e) => {
    e.preventDefault()
    const text = this.state.text
    this.props.send(text)
    this.setState({ text: "" })
  }
  render() {

    const { old_messages, friends } = this.props;
    return (
      <div className="container">
        <h3 className=" text-center">Codi Chat Room</h3>
        <h5>the sockectChallenge</h5>
        <div className="messaging">
          <div className="inbox_msg">
            <div className="inbox_people">
              <div className="headind_srch">
                <div className="recent_heading">
                  <h4>{friends.length} people are <span className="online">online</span></h4>
                </div>
                <div className="srch_bar">
                  <div className="stylish-input-group">
                    <input type="text" className="search-bar" placeholder="Search" />
                    <span className="input-group-addon">
                      <button type="button"> <i className="fa fa-search" aria-hidden="true" /> </button>
                    </span> </div>
                </div>
              </div>

              <div className="inbox_chat">


                <div className="chat_list active_chat">

                  {friends.map(friend =>
                    <div>

                      <div className="chat_people">
                        <div className="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil" /> </div>
                        <div className="chat_ib">
                          <h5>{friend}
                          </h5>
                          {/* <span className="chat_date">Dec 25</span>
                          <p>Test, which is a new approach to have all solutions
                                            astrology under one roof.</p> */}
                        </div>
                      </div>
                      <hr />
                    </div>
                  )}
                  <div>
                  </div>

                </div>


              </div>
            </div>

            <div className="mesgs" id="your_div">
              <div className="msg_history">

                {/* incoming_msg */}
                {old_messages.map(msg => {
                  if (msg.name === "husseinh") {
                    return (<div className="incoming_msg">
                      <div className="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil" /> </div>
                      <div className="received_msg">
                        <div className="received_withd_msg">
                          <h5 className="sender">Hussein H</h5>
                          <p>{msg.text}</p>
                          <span className="time_date"> {msg.date}</span></div>
                      </div>
                    </div>)
                  }
                  else {
                    return (
                      <div className="outgoing_msg">
                        <div className="sent_msg">
                          <h5 className="sender">{msg.name} : </h5>
                          <p>{msg.text}</p>
                          <span className="time_date">{msg.date}</span>
                        </div>
                      </div>
                    )
                  }
                }
                )}
                {/* outgoing_msg */}
              </div>
              <div className="type_msg">
                <div className="input_msg_write">
                  <form onSubmit={this.onSubmit}>
                    <input type="text" value={this.state.text} className="write_msg" placeholder="Type a message" onChange={e => this.setState({ text: e.target.value })} />
                    <button className="msg_send_btn" type="submit"><i className="fa fa-paper-plane-o" aria-hidden="true" /></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <p className="text-center top_spac"> made by  <a target="_blank" href="#">Hussein H</a></p>
          <p className="text-center top_spac"> Thank you <a target="_blank" href="#">Jad</a> this is really entertainment,beneficial and tought me alot </p>

        </div>
      </div>
    );
  }
}


export default Chat;